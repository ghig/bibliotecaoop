package pucrs.progoo;

public class Midia extends Obra implements Emprestavel{


	private String tipoDeItem;
	private int quantidade;
	
	public Midia(int codigo, String titulo, String editora, int ano, String tipoDeItem, int quantidade) {
		super(codigo, titulo, editora, ano);
		this.tipoDeItem = tipoDeItem;
		this.quantidade = quantidade;
	}

	public String getTipoDeItem() {
		return tipoDeItem;
	}

	public void setTipoDeItem(String tipoDeItem) {
		this.tipoDeItem = tipoDeItem;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	
	

	@Override
	public boolean emprestar() {
		if(quantidade != 0){
			quantidade--;
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean devolver() {
		quantidade++;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + " - " + tipoDeItem + " - " + quantidade;
	}

	
	
}
