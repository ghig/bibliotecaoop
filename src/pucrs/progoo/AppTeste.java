package pucrs.progoo;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class AppTeste {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Acervo acervo = new Acervo();
		Cadastro cadastro = new Cadastro();
		Emprestimos listaEmp = new Emprestimos();

		cadastro.cadastrar("1", "Fulano", "01929123132");
		cadastro.cadastrar("2", "Beltrano", "82816612122");
		cadastro.cadastrar("3", "Ciclano", "91277626212");
		cadastro.cadastrar("4", "Huguinho", "28156483929");
		cadastro.cadastrar("5", "Zezinho", "71626846671");
		cadastro.cadastrar("6", "Luizinho", "77691761727");
		cadastro.ordenaNome();

		acervo.cadastrarLivro(new Livro(1, "O Senhor dos Anéis", "J. R. R. Tolkien", "Martins Fontes", 2000, 5));
		acervo.cadastrarLivro(new Livro(2, "A Origem das Espécies", "Charles Darwin", "Hemus", 2000, 2));
		acervo.cadastrarLivro(new Livro(3, "A Arte da Guerra", "Sun Tzu", "Pensamento", 1995, 3));
		acervo.cadastrarLivro(new Livro(4, "Memórias Póstumas de Brás Cubas", "Machado de Assis", "Ática", 1998));
		acervo.cadastrarLivro(new Livro(5, "O Senhor dos Anéis", "J. R. R. Tolkien", "Martins Fontes", 1958, 1));
		acervo.ordenaTitulo();

		Boolean usuario = Boolean.TRUE;

		Scanner in = new Scanner(System.in);

		while (usuario) {

			System.out.println("Digite o código do usuário");
			String codigo = in.nextLine();
			Usuario u = cadastro.buscarCodigo(codigo);

			ArrayList<Emprestimo> listaEmprestimos = listaEmp.buscarPorUsuario(u);

			for (Emprestimo e : listaEmprestimos) {
				System.out.println("Emprestimos: ");
				e.toString();
				if (e.getDataDevolucao() == null) {
					System.out.println("Você deseja devolver?(S - Sim, N - Não)");
					String resposta = in.nextLine();
					if (resposta.equalsIgnoreCase("S")) {
						e.getLivro().devolver();
						e.setDataDevolucao(LocalDate.now());
					}
				}
			}

			Boolean locarLivro = Boolean.TRUE;

			while (locarLivro) {
				System.out.println("Livros disponíveis: ");
				acervo.listarTodos();
				System.out.println("Deseja locar algum livro? Digite o código:");
				String codeString = in.nextLine();
				int code = Integer.parseInt(codeString);

				System.out.println("Digite a data de devolução (formato dd/mm/yyyy):");
				String dataDev = in.nextLine();

				DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

				LocalDate dataEMpresitomo = null;

				if (dataDev != null) {
					try {
						dataEMpresitomo = LocalDate.parse(dataDev, dtFormatter);
					} catch (Exception e) {
						System.out.println("Formato inválido.");
					}

					if (dataEMpresitomo == null) {
						System.out.println("Formato inválido.");
					}

				} else {
					System.out.println("Data de devolução inválida");
				}

				Livro livro = acervo.buscarPorCodigo(code);

				if (livro != null) {
					livro.emprestar();
					listaEmp.criar(u, livro, dataEMpresitomo);
					
				} else {
					System.out.println("Livro não encontrado!");
				}

				System.out.println("Deseja locar outro livro para este usuário? (S-SIM N-NAO)");
				String locar = in.nextLine();

				if (locar.equalsIgnoreCase("N")) {
					locarLivro = Boolean.FALSE;
				}

			}

			System.out.println("Deseja realizar a operação para outro usuário? (S-SIM N-NAO)");
			String outroUsuario = in.nextLine();

			if (outroUsuario.equalsIgnoreCase("n")) {
				usuario = Boolean.FALSE;
			}

		}

	}

}
